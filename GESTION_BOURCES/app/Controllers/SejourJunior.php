<?php

namespace App\Controllers;
use App\Models\Junior_Model;
use ZipArchive;

class SejourJunior extends BaseController
{
    
	public function __construct()
	{
		$db = \Config\Database::connect();
		//$db = \Config\Database::connect();

	}
	public function index()
	{
		return (view('base') . view('junior') . view('foolter'));
	}

	public function voir()
	{
		$allocation_Model = new Junior_Model();
		$donnees['junior'] = $allocation_Model->findAll();
		return (view('base') . view("templates/junior", $donnees) . view('foolter'));
	}
	public function encad()
	{
		$allocation_Model = new Junior_Model();
		$donnees['junior'] = $allocation_Model->findAll();
		return (view('base') . view("templates/junior_encad", $donnees) . view('foolter'));
	}
	public function voirfiles()
	{
		$allocation_Model = new Junior_Model();
		$donnees['junior'] = $allocation_Model->findAll();
		return (view('base') . view("templates/junior_files", $donnees) . view('foolter'));
	}


	public function ajout()
	{
		$model_promotion = new Junior_Model();

		$nom = $this->request->getVar('nom');
		$prenom = $this->request->getVar('prenom');
		$dateNaissance = $this->request->getVar('dataNaissance');
		$email = $this->request->getVar('email');
		$telephone = $this->request->getVar('telephone');
		$NNI = $this->request->getVar('NNI');
		$genre = $this->request->getVar('genre');
		$adresse = $this->request->getVar('adresse');

		$data1 = [
			'nom' => $nom,
			'prenom' => $prenom,
			'NNI' => $NNI,
			'genre' => $genre,
			'dateNaissance' => $dateNaissance,
			'telephone' => $telephone,
			'email' => $email,
			'adresse' => $adresse
		];

		$projetR = $this->request->getPost('projetR');
		$resumePR = $this->request->getPost('resumePR');
		$motif = $this->request->getPost('motif');
		$nomDRM = $this->request->getPost('nomDRM');
		$nomSAE = $this->request->getPost('nomSAE');
		$directeurE = $this->request->getPost('directeurE');
		$emailE = $this->request->getPost('emailE');

		$data2 = [
			'projetR' => $projetR,
			'resumePR' => $resumePR,
			'motif' => $motif,
			'nomDRM' => $nomDRM,
			'nomSAE' => $nomSAE,
			'directeurE' => $directeurE,
			'emailE' => $emailE
		];

		$photo = $this->request->getFile('photo');
		$passport = $this->request->getFile('passport');
		$dipDoctorat = $this->request->getFile('dipDoctorat');
		$cv = $this->request->getFile('cv');
		$lettreInv = $this->request->getFile('lettreInv');
		$autorAbsence = $this->request->getFile('autorAbsence');

		$data3 = [
			'photo' => $photo->getClientMimeType(),
			'passport' => $passport->getClientMimeType(),
			'dipDoctorat' => $dipDoctorat->getClientMimeType(),
			'cv' => $cv->getClientMimeType(),
			'lettreInv' => $lettreInv->getClientMimeType(),
			'autorAbsence' => $autorAbsence->getClientMimeType(),
		];

		// C R U D
		// Ajout -> insert
		$model_promotion->insert($data1 + $data2 + $data3);

		$id = $model_promotion->getInsertID();
		$photo->move(WRITEPATH . 'uploads/junior/' . $id, 'photo' . $id . '.pdf');
		$passport->move(WRITEPATH . 'uploads/junior/' . $id, 'passport' . $id . '.pdf');
		$dipDoctorat->move(WRITEPATH . 'uploads/junior/' . $id, 'dipDoctorat' . $id . '.pdf');
		$cv->move(WRITEPATH . 'uploads/junior/' . $id, 'cv' . $id . '.pdf');
		$lettreInv->move(WRITEPATH . 'uploads/junior/' . $id, 'lettreInv' . $id . '.pdf');
		$autorAbsence->move(WRITEPATH . 'uploads/junior/' . $id, 'autorAbsence' . $id . '.pdf');
		
		$this->generateZipFile($id);
		printf("Les données ont été ajoutées avec succès.");
		return (view('base') . view('junior') . view('foolter'));
	}


	public function downloadFile($nom, $id)
	{
		$fileName = $nom . $id;

		return $this->response->download(WRITEPATH . 'uploads/junior/' . $id . '/' . $fileName . '.pdf', null);
	}
	public function downloadZipFile($id)
	{
		return $this->response->download(WRITEPATH . 'uploads/junior/' . $id . '.zip', null);
	}

	public function generateZipFile($id)
	{
		// Create a new zip file
		$zip = new ZipArchive();
		$zipFileName = WRITEPATH . 'uploads/junior/' . $id . '.zip';
		$zip->open($zipFileName, ZipArchive::CREATE | ZipArchive::OVERWRITE);

		// Get the user's directory path
		$userDir = WRITEPATH . 'uploads/junior/' . $id;

		// Iterate through the user's files and add them to the zip file
		foreach (glob($userDir . '/*') as $file) {
			$zip->addFile($file, basename($file));
		}

		// Close the zip file
		$zip->close();
	}

}