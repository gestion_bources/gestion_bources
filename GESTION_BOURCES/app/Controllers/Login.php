<?php

namespace App\Controllers;
use App\Models\UserModel;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\RedirectResponse;
use CodeIgniter\Controller;

class Login extends BaseController
{
    public function __construct()
    {
        $db = \Config\Database::connect();
        //$db = \Config\Database::connect();

    }
    public function index()
    {		
		$data = [];
		helper(['form']);  
		if ($this->request->getPost()) {
			
			//let's do the validation here
			$rules = [
				'email' => 'required|min_length[2]|max_length[50]|valid_email',
				'password' => 'required|min_length[2]|max_length[255]|validateUser[email,password]',
			];

			$errors = [
				'password' => [
					'validateUser' => 'Email or Password don\'t match'
				]
			];

			if (! $this->validate($rules, $errors)) {
				$data['validation'] = $this->validator;
				 
			} else {
				$model = new UserModel();

				$user = $model->where('email', $this->request->getVar('email'))
					->first();

				$this->setUserSession($user);
				//$session->setFlashdata('success', 'Successful Registration');
				return (view('base').view('home',$data).view('foolter'));
			}
		}

        
				echo view('base');
				echo view('login',$data) ;
				echo view('foolter');
    }

	public function register()
	{
		$data = [];
		helper(['form']);
		if ($this->request->getPost()) {
			//let's do the validation here
			$rules = [
				'firstname' => 'required|min_length[3]|max_length[20]',
				'lastname' => 'required|min_length[3]|max_length[20]',
				'email' => 'required|min_length[6]|max_length[50]|valid_email|is_unique[users.email]',
				'password' => 'required|min_length[8]|max_length[255]',
				'password_confirm' => 'matches[password]',
			];

			//if (!$this->validate($rules)) {
			// 	$data['validation'] = $this->validator;
			// } else {
			// 	$model = new UserModel();

			// 	$newData = [
			// 		'firstname' => $this->request->getVar('firstname'),
			// 		'lastname' => $this->request->getVar('lastname'),
			// 		'email' => $this->request->getVar('email'),
			// 		'password' => $this->request->getVar('password'),
			// 	];
			// 	$model->save($newData);
			// 	$session = session();
			// 	$session->setFlashdata('success', 'Successful Registration');
			// 	return redirect()->to('/');

			// }
			$model = new UserModel();

				$newData = [
					'firstname' => $this->request->getVar('firstname'),
					'lastname' => $this->request->getVar('lastname'),
					'email' => $this->request->getVar('email'),
					'password' => $this->request->getVar('password'),
				];
				$model->save($newData);
				$session = session();
				$session->setFlashdata('success', 'Successful Registration');
				//return redirect()->to('/');
		}
		echo view('base');
		echo view('register',$data) ;
		echo view('foolter');
	}
    private function setUserSession($user){
		$data = [
			'id' => $user['id'],
			'firstname' => $user['firstname'],
			'lastname' => $user['lastname'],
			'email' => $user['email'],
			'isLoggedIn' => true,
		];

		session()->set($data);
		return true;
	}

	public function logout(){
		session()->destroy();
		return redirect()->to('/');
	}
}