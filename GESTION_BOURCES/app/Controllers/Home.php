<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {   
        $title = [
            'home_title' => 'home',
            'base_title' => 'base',
            'foolter_title' => 'foolter',
        ];
        echo view('base');
        echo view('home', $title );
        echo view('foolter');
    }
}
