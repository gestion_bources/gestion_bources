<?php

namespace App\Controllers;

use App\Models\Allocation_Model;
use ZipArchive;

class AllocationDoctorat extends BaseController
{

	public function __construct()
	{
		$db = \Config\Database::connect();
		//$db = \Config\Database::connect();

	}
	public function index()
	{
		/*
		$model_promotion = new PromotionModel();
		
		// C R U D
		// Ajout -> insert
		$donnees = ['intitule' => "BAC 1"];
		$model_promotion->insert($donnees);
		// Modification -> update
		$donnees = ['intitule' => 'G1'];
		$model_promotion->update(2, $donnees);
		// Suppression -> delete
		$model_promotion->delete(1);
		// Lecture -> find, findAll
		$promotions = $model_promotion->findAll();
		var_dump($promotions);
		*/
		return (view('base') . view('allocation') . view('foolter'));
	}

	public function accueil()
	{
		$model_promotion = new Allocation_Model();
		$donnees['total_promotions'] = count($model_promotion->findAll());

		return (view('base') . view('allocation') . view('foolter'));
	}

	
	public function voir()
	{
		$allocation_Model = new Allocation_Model();
		$donnees['allocation'] = $allocation_Model->findAll();
		return (view('base') . view("templates/allocation", $donnees) . view('foolter'));
	}
	public function encad()
	{
		$allocation_Model = new Allocation_Model();
		$donnees['allocation'] = $allocation_Model->findAll();
		return (view('base') . view("templates/allocation_encad", $donnees) . view('foolter'));
	}
	public function voirfiles()
	{
		$allocation_Model = new Allocation_Model();
		$donnees['allocation'] = $allocation_Model->findAll();
		return (view('base') . view("templates/allocation_files", $donnees) . view('foolter'));
	}


	public function ajout()
	{
		$model_promotion = new Allocation_Model();

		$nom = $this->request->getVar('nom');
		$prenom = $this->request->getVar('prenom');
		$dateNaissance = $this->request->getVar('dataNaissance');
		$email = $this->request->getVar('email');
		$telephone = $this->request->getVar('telephone');
		$NNI = $this->request->getVar('NNI');
		$genre = $this->request->getVar('genre');
		$adresse = $this->request->getVar('adresse');

		$data1 = [
			'nom' => $nom,
			'prenom' => $prenom,
			'NNI' => $NNI,
			'genre' => $genre,
			'dateNaissance' => $dateNaissance,
			'telephone' => $telephone,
			'email' => $email,
			'adresse' => $adresse
		];

		$titre = $this->request->getPost('titrethese');
		$resume = $this->request->getPost('resume');
		$directeur = $this->request->getPost('directeur');
		$grade = $this->request->getPost('grade');
		$structure = $this->request->getPost('structure');
		$emailD = $this->request->getPost('emailD');
		$directeurF = $this->request->getPost('directeurF');
		$gradeF = $this->request->getPost('gradeF');
		$affilation = $this->request->getPost('affilation');
		$emailT = $this->request->getPost('emailT');

		$data2 = [
			'titrethese' => $titre,
			'resume' => $resume,
			'directeur' => $directeur,
			'grade' => $grade,
			'structure' => $structure,
			'emailD' => $emailD,
			'directeurF' => $directeurF,
			'gradeF' => $gradeF,
			'affilation' => $affilation,
			'emailT' => $emailT
		];

		$photo = $this->request->getFile('photo');
		$passport = $this->request->getFile('passport');
		$attestation = $this->request->getFile('attestation');
		$masterDip = $this->request->getFile('masterDip');
		$noteLM = $this->request->getFile('noteLM');
		$attestB2 = $this->request->getFile('attestB2');
		$lettreEng = $this->request->getFile('lettreEng');
		$cvDoctorat = $this->request->getFile('cvDoctorat');

		$data3 = [
			'photo' => $photo->getClientMimeType(),
			'passport' => $passport->getClientMimeType(),
			'attestation' => $attestation->getClientMimeType(),
			'masterDip' => $masterDip->getClientMimeType(),
			'noteLM' => $noteLM->getClientMimeType(),
			'attestB2' => $attestB2->getClientMimeType(),
			'lettreEng' => $lettreEng->getClientMimeType(),
			'cvDoctorat' => $cvDoctorat->getClientMimeType()
		];

		// C R U D
		// Ajout -> insert
		$model_promotion->insert($data1 + $data2 + $data3);

		$id = $model_promotion->getInsertID();
		$photo->move(WRITEPATH . 'uploads/allocation/' . $id, 'photo' . $id . '.pdf');
		$passport->move(WRITEPATH . 'uploads/allocation/' . $id, 'passport' . $id . '.pdf');
		$attestation->move(WRITEPATH . 'uploads/allocation/' . $id, 'attestation' . $id . '.pdf');
		$masterDip->move(WRITEPATH . 'uploads/allocation/' . $id, 'masterDip' . $id . '.pdf');
		$noteLM->move(WRITEPATH . 'uploads/allocation/' . $id, 'noteLM' . $id . '.pdf');
		$attestB2->move(WRITEPATH . 'uploads/allocation/' . $id, 'attestB2' . $id . '.pdf');
		$lettreEng->move(WRITEPATH . 'uploads/allocation/' . $id, 'lettreEng' . $id . '.pdf');
		$cvDoctorat->move(WRITEPATH . 'uploads/allocation/' . $id, 'cdDoctorat' . $id . '.pdf');

		$this->generateZipFile($id);
		printf("Les données ont été ajoutées avec succès.");
		return (view('base') . view('allocation') . view('foolter'));
	}


	public function downloadFile($nom, $id)
	{
		$fileName = $nom . $id;

		return $this->response->download(WRITEPATH . 'uploads/allocation/' . $id . '/' . $fileName . '.pdf', null);
	}
	public function downloadZipFile($id)
	{
		echo WRITEPATH . 'uploads/allocation/' . $id . '.zip';
		return $this->response->download(WRITEPATH . 'uploads/allocation/' . $id . '.zip', null);
	}

	public function generateZipFile($id)
	{
		// Create a new zip file
		$zip = new ZipArchive();
		$zipFileName = WRITEPATH . 'uploads/allocation/' . $id . '.zip';
		$zip->open($zipFileName, ZipArchive::CREATE | ZipArchive::OVERWRITE);

		// Get the user's directory path
		$userDir = WRITEPATH . 'uploads/allocation/' . $id;

		// Iterate through the user's files and add them to the zip file
		foreach (glob($userDir . '/*') as $file) {
			$zip->addFile($file, basename($file));
		}

		// Close the zip file
		$zip->close();
	}

}