<head>
  <meta charset='utf-8'>
  <meta http-equiv='X-UA-Compatible' content='IE=edge'>
  <title>SCAC et ANRSI </title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS only -->
  <!-- JavaScript Bundle with Popper -->

  <link type="text/css" href="css/mystyle.css" rel="stylesheet" media="all">
  <link type="text/css" href="css/carousel.css" rel="stylesheet" media="all">
  <link type="text/css" href="css/bootstrap.css" rel="stylesheet" media="all">
  <link type="text/css" href="css/bootstrap.min.css" rel="stylesheet" media="all">
  <link type="text/css" href="css/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet" media="all">
</head>

<body>
<?php
      $uri = service('uri');
     ?>
  <nav class="navbar navbar-expand-sm bg-light navbar-light">
    <div class="container-fluid">
      <a class="navbar-brand active" href="/">
        <h3><img src="static/arnsi.jpg" width="150" height="75" class="brand-image img-circle elevation-3"
            style="opacity: .8" alt="#">
          <span>
            SCAC et ANRSI
          </span>
        </h3>
      </a>

      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="#">Qui sommes nous ?</a>
        </li>
        <!-- <li class="nav-item">
          <a class="nav-link  border " href="signup">Inscription</a>
        </li> -->
        <?php if (session()->get('isLoggedIn')): ?>
        
          <li class="nav-item">
          <a class="nav-link" href="logout"><i class="bi bi-box-arrow-right  " style="color: brown ;"></i> <?= session()->get('firstname') ?></a>
        </li>
          <?php else: ?>
         <li class="nav-item <?= ($uri->getSegment(1) == 'register' ? 'active' : null) ?>">
          <a class="nav-link" href="login">Connexion</a>
        </li>
        <li class="nav-item <?= ($uri->getSegment(1) == 'register' ? 'active' : null) ?>">
          <a class="nav-link  border " href="signup">Inscription</a>
        </li>
        <?php endif; ?>
      </ul>
    </div>
  </nav>
</body>
<br>