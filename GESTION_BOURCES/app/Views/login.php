<div class="container" id="login-container">
  <div><br>
    <h1>Se connecter</h1><br>
  </div>
  <?php if (session()->get('success')): ?>
          <div class="alert alert-success" role="alert">
            <?= session()->get('success') ?>
          </div>
        <?php endif; ?>
  <form method="post" action="login">
    <div class="form-floating">
      <input type="email" class="form-control" id="floatingInput" value="<?= set_value('email') ?>" name="email" placeholder="name@example.com">
      <label for="floatingInput" class="opacity-50">Address mail</label>
    </div><br>
    <div class="form-floating">
      <input type="password" name="password" value="<?= set_value('password') ?>" class="form-control" id="floatingPassword" placeholder="Password">
      <label for="floatingPassword" class="opacity-50">Mot de passe</label>
    </div>
<br><br>
<?php if (isset($validation)): ?>
            <div class="col-12">
              <div class="alert alert-danger" role="alert">
                <?= $validation->listErrors() ?>
              </div>
            </div>
          <?php endif; ?>
    <button class="w-100 btn btn-lg btn-outline-primary" type="submit">connection</button>
    <p>Vous n'avez pas un compte <a href="signup"> inscrire</a></p>

  </form>

</div>

<style>
  #login-container {
    width: 400px;
    margin: auto;
    text-align: center;
  }
 
</style>