<div class="container"> <br>
  <div class="card-header">
    <h2 class="card-title text-center">LISTES DES BOURSES</h2>
  </div> <br> <!-- Default box --> <br>
  <div class="container"> <br>
    <div class="row">
      <!-- /.col-md-6 -->
      <div class="col-lg-6">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title m-0">ALLOCATION DOCTORALE</h5>
          </div>
          <div class="card-body">
            <h6 class="card-title">
              <!--Special title treatment-->
            </h6>
            <p class="card-text">
            <details>
              <summary>Cette allocation s'dresse à des étudiants mauritaniens qui souhaitent réaliser une thèse ou
                qui sont actuellement en première année de thèse. </summary> La thèse doit se réaliser entre un
              laboratoire de recherche mauritanien et français. Le doctorant bénéficiera dune bourse de mobilité de
              6 mois par an, pendant 3 ans, dans un laboratoire français et en cotutelle (sous réserve de sa
              réinscription en thèse). La bourse comprend pour chaque séjour, les frais de transport international,
              d'hébergement, de subsistance, ainsi que la couverture sociale et les frais d'inscription
              universitaires en France. Les frais pédagogiques (i.e., frais de paillasse, frais relatifs à
              l'inscription à un colloque, frais de formation, …) ne sont pas pris en charge. Avant de renseigner ce
              formulaire asurez-vous d'avoir les pièces joindre suivantes au format PDF d'une taille inférieure à 2
              Mo : copie du passeport dont la validité est au minimum jusqu'au 1 mai 2023. CV du doctorant CV des 2
              encadrants avec leurs listes de publications Attestation d'inscription en thèse en mauritanie Copie du
              diplôme ou attestation de master ou équivalent Relevès des Notes Licence et Master Attestation delf B2
              ou TCF Lettre d'engagement des 2 directeurs de thèse (utiliser obligatoirement la fiche d'engagement
              jointe)
            </details>
            </p> <a href="allocation" class="btn btn-outline-primary ">Soummettre un dossier</a>
          </div>
        </div> <br>

        <div class="card card-primary card-outline">
          <div class="card-header">
            <h5 class="card-title m-0">SEJOUR DE RECHERCHE JUNIOR</h5>
          </div>
          <div class="card-body">
            <h6 class="card-title">
              <!--Special title treatment-->
            </h6>
            <p class="card-text">
            <details>
              <summary>Cette allocation s'adresse à des enseignants chercheurs ou des chercheurs mauritaniens
                actuellement en poste en Mauritanie qui souhaiteraient bénéficier d'une mobilité de 2 mois maximum
                dans un laboratoire étranger,</summary> dans le cadre d'une activité de recherche, d'un programme de
              coopération scientifique ou de la préparation d'une HDR. Les frais pédagogiques (i.e., frais de
              paillasse, frais relatifs à l'inscription à un colloque, frais de formation, …) ne sont pas pris en
              charge. Avant de renseigner ce formulaire asurez-vous d'avoir les pièces à joindre suivantes au format
              PDF d'une taille inférieure à 2 Mo : copie du passeport dont la validité minimum est jusqu'au 1 mai
              2023 Copie de votre diplôme de doctorat ou équivalent CV avec mention de la liste de vos publications
              Lettre d'invitation de la structure d'accueil avec mention des dates de séjour Autorisation d'absence
              de votre université ou de votre établissement
            </details>
            </p> <a href="junior" class="btn btn-outline-primary ">Soummettre un dossier</a>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title m-0">SEJOUR DE RECHERCHE SENIOR</h5>
          </div>
          <div class="card-body">
            <h6 class="card-title">
              <!--Special title treatment-->
            </h6>
            <p class="card-text">
            <details>
              <summary>Cette allocation s'adresse à des enseignants chercheurs ou des chercheurs confirmés
                mauritaniens, actuellement en poste en Mauritanie qui souhaiteraient bénéficier d'une mobilité de 1
                mois maximum dans un laboratoire étranger,</summary> pour mettre en place une coopération
              scientifique internationale ou finaliser un travail de recherche. Les frais pédagogiques (i.e., frais
              de paillasse, frais relatifs à l'inscription à un colloque, frais de formation, …) ne sont pas pris en
              charge. Avant de renseigner ce formulaire asurez-vous d'avoir les pièces à joindre suivantes au format
              PDF d'une taille inférieure à 2 Mo : Copie du passeport dont la validité est au minimum au 1 mai 2023
              Copie de votre diplôme de doctorat ou équivalent CV avec mention de la liste de vos publications
              Lettre d'invitation de la structure d'accueil avec mention des dates de séjour Autorisation d'absence
              de votre établissement.
            </details>
            </p> <a href="#" class="btn btn-outline-primary ">Soummettre un dossier</a>
          </div>
        </div> <br>
        <div class="card card-primary card-outline">
          <div class="card-header">
            <h5 class="card-title m-0">SEJOUR DOCTORAL</h5>
          </div>
          <div class="card-body">
            <h6 class="card-title">
              <!--Special title treatment-->
            </h6>
            <p class="card-text">
            <details>
              <summary>Cette allocation s'adresse à des étudiants mauritaniens qui souhaitent réaliser une thèse ou
                qui sont actuellement en première année de thèse et qui souhaite bénéficier d'une bourse de mobilité
                de 4 mois maximal, dans un laboratoire étranger.</summary> Les frais pédagogiques (i.e., frais de
              paillasse, frais relatifs à l'inscription à un colloque, frais de formation …) ne sont pas pris en
              charge. Avant de renseigner ce formulaire asurez-vous d'avoir les pièces joindre suivantes au format
              PDF d'une taille inférieure à 2 Mo : copie du passeport dont la validité est au minimum jusqu'au 1 mai
              2023. CV du doctorant CV des 2 encadrants avec leurs listes de publications Attestation d'inscription
              en thèse en mauritanie. Copie du diplôme ou attestation de master ou équivalent Relevès des Notes
              Licence et Master. Une attestation d'accueil dans le laboratoire étranger précisant les dates du
              séjour.
            </details>
            </p> <a href="#" class="btn btn-outline-primary ">Soummettre un dossier</a>
          </div>
        </div>
      </div> <!-- /.col-md-6 -->
    </div> <br> <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
