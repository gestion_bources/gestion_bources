def replace_words(input_file,old_word, new_word):
  # Open the input file in read mode
  with open(input_file, 'r') as infile:
    # Read the contents of the file
    text = infile.read()

  # Split the text into a list of words
  words = text.split()

  # Iterate over the list of words and replace each word with the new word
  for i, word in enumerate(words):
    words[i] = word.replace(old_word, new_word)
  # Join the list of words back into a single string
  modified_text = ' '.join(words)

  # Open the output file in write mode
  with open(input_file, 'w') as outfile:
    # Write the modified text to the output file
    outfile.write(modified_text)

# Example usage
replace_words('allocation.php', 'id=', '"')

