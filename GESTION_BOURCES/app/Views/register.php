<div class="container" id="login-container">
  <div><br>
    <h1>register</h1><br>
  </div>
  <form class="" action="/register" method="post">
      <div class="form-group" align="left">
        <label for="firstname" class="ms-3" >First Name</label>
        <input type="text" class="form-control" name="firstname" id="firstname" value="<?= set_value('firstname') ?>" required>
      </div>
      <div class="form-group" align="left">
        <label for="lastname" class="ms-3">Last Name</label>
        <input type="text" class="form-control" name="lastname" id="lastname" value="<?= set_value('lastname') ?>" required>
      </div>


      <div class="form-group" align="left">
        <label for="email" class="ms-3">Email address</label>
        <input type="text" class="form-control" name="email" id="email" value="<?= set_value('email') ?>" required>
      </div>

      <div class="form-group" align="left">
        <label for="password" class="ms-3">Password</label>
        <input type="password" class="form-control" name="password" id="password" value="">
      </div>

      <div class="form-group" align="left">
        <label for="password_confirm" class="ms-3">Confirm Password</label>
        <input type="password" class="form-control" name="password_confirm" id="password_confirm" value="" required>
      </div>

      <?php if (isset($validation)): ?>
        <div class="col-12">
          <div class="alert alert-danger" role="alert">
            <?= $validation->listErrors() ?>
          </div>
        </div>
      <?php endif; ?>
 
    <button class="w-100 btn btn-lg btn-outline-primary" type="submit">Inscrire</button>
    <p>
      Vous avez un compte <a href="login"> connecter </a>
    </p>
  </form>
</div>
<style>
  #login-container {
    width: 400px;
    margin: auto;
    text-align: center;
  }
</style>
 