<div class="container"> <br>
    <div class="card-header">
        <h3 class="card-title text-center">SEJOUR DE RECHERCHE JUNIOR</h3>
    </div> <br> <!-- Default box --> <br>
    <form method="post" action="junior/ajout" enctype="multipart/form-data" onsubmit="return validateForm()">
        <style>
            .nav {
                display: flex;
                justify-content: space-between;
                align-items: center;
                justify-content: center;

            }

            .nav-link {
                flex: 0 0 auto;
            }
        </style>
        <div id="page-1">
            <!-- form fields for page 1 go here -->
            <div class="container card"><br>
                <div class="container-fluid ">
                    <h5>
                        <ul class="nav nav-tabs">
                            <li class="nav-item"> <a class="nav-link text-info opacity-100 active"
                                    aria-current="page">Identite du candidat</a> </li>
                            <li class="nav-item"> <a class="nav-link text-secondary opacity-50">Information sur la
                                    projet de recherche</a> </li>
                            <li class="nav-item"> <a class="nav-link text-secondary opacity-50 ">Information accueil</a>
                            </li>
                            <li class="nav-item"> <a class="nav-link text-secondary opacity-50">Piece a joindre</a>
                            </li>
                        </ul>
                    </h5>
                </div>
                <br>
                <div class="container spacer  col-xs-12 col-md-offset-3">
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group"> <label for="first-name">Nom</label> <input type="text"
                                    class="form-control" name="nom" placeholder="Enter first name" required> </div>
                            <div class="form-group"> <label for="last-name">Premon</label> <input type="text"
                                    class="form-control" name="prenom" placeholder="Enter last name" required> </div>
                            <div class="form-group"> <label for="birthday">Date de naissance</label> <input type="date"
                                    class="form-control" name="dataNaissance" required> </div>
                            <div class="form-group" id="genre"> <label>Civilité *</label>
                                <div class="form-check"> <input class="form-check-input" type="radio" name="genre"
                                        value="femme"> <label class="form-check-label">Madame</label> </div>
                                <div class="form-check"> <input class="form-check-input" type="radio" name="genre"
                                        checked value="homme"> <label class="form-check-label">Monsieur</label> </div>
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group"> <label for="email">Email</label> <input type="email"
                                    class="form-control" name="email" placeholder="Enter email"> </div>

                            <div class="form-group"> <label for="phone-number">Phone Number</label> <input type="tel"
                                    maxlength="20" class="form-control" name="telephone"
                                    placeholder="Enter phone number"> </div>
                            <div class="form-group"> <label for="national-number">National Number</label> <input
                                    type="number" maxlength="20" class="form-control" name="NNI"
                                    placeholder="Enter national number"> </div>
                            <div class="form-group"> <label for="adresse">Adresse</label> <input type="text"
                                    class="form-control" name="adresse" placeholder="Enter your adress"> </div>
                        </div>
                    </div>

                </div>
            </div> <br><button class="btn btn-outline-primary" type="button" onclick="nextPage()">Next</button>
        </div>
        <div id="page-2" style="display: none;">
            <!-- form fields for page 2 go here -->
            <div class="container card"><br>
                <div class="container-fluid">
                    <h5>
                        <ul class="nav nav-tabs">
                            <li class="nav-item"> <a class="nav-link text-secondary opacity-50"
                                    aria-current="page">Identite du candidat</a> </li>
                            <li class="nav-item"> <a class="nav-link text-info opacity-100 active ">Information sur la
                                    projet de recherche</a> </li>
                            <li class="nav-item"> <a class="nav-link text-secondary opacity-50 ">Information accueil</a>
                            </li>
                            <li class="nav-item"> <a class="nav-link text-secondary opacity-50">Piece a joindre</a>
                            </li>
                        </ul>
                    </h5>
                </div>
                <br>
                <div class="container spacer col-md-6 col-xs-12 col-md-offset-3">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form method="post" action="saveEtudiant.php" enctype="multipart/form-data">
                                <div class="form-group"> <label class="control-label">Projet de recherche</label> <input
                                        type="text" name="projetR" class="form-control" /> </div>
                                <div class="mb-3"> <label for="exampleFormControlTextarea1" class="form-label">Résumé du
                                        projet de recherche </label> <textarea class="form-control"
                                        id="exampleFormControlTextarea1" rows="3" name="resumePR"></textarea> </div>
                                <div class="mb-3"> <label for="exampleFormControlTextarea1" class="form-label">Motif de
                                        la mobilité internationale dans le cadre de votre projet de recherche </label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="motif"></textarea>
                                </div>
                        </div>
                    </div>
                </div>
            </div> <br><button type="button" class="btn btn-outline-primary" onclick="prevPage()">Prev</button> <button
                type="button" class="btn btn-outline-primary" onclick="nextPage()">Next</button>
        </div>
        <div id="page-3" style="display: none;">
            <div class="container card">
                <!-- form fields for page 3 go here --> <br>
                <div class="container-fluid">
                    <h5>
                        <ul class="nav nav-tabs">
                            <li class="nav-item"> <a class="nav-link text-secondary opacity-50">Identite du candidat</a>
                            </li>
                            <li class="nav-item"> <a class="nav-link text-secondary opacity-50 ">Information sur la
                                    projet de recherche</a> </li>
                            <li class="nav-item"> <a class="nav-link text-info opacity-100 active"
                                    aria-current="page">Information accueil</a> </li>
                            <li class=" nav-item"> <a class="nav-link text-secondary opacity-50">Piece a joindre</a>
                            </li>
                        </ul>
                    </h5>
                </div>
                <br>
                <div class="container spacer col-xs-12 col-md-offset-3">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md">
                                    <div class="form-group"> <label class="control-label ">Nom de votre Structure de
                                            recherche d'affiliation en Mauritanie</label> <input type="text" name="nomSRM"
                                            class="form-control" /> </div>
                                    <div class="form-group"> <label class="control-label ">Nom de votre structure
                                            d'accueil à l'étranger</label> <input type="text" name="nomSAE"
                                            class="form-control" /> </div>
                                </div>
                                <div class="col-md">
                                    <div class="form-group"> <label class="control-label ">Nom et prénom du directeur de
                                            votre structure d'accueil à l'étranger</label> <input type="text"
                                            name="directeurE" class="form-control"  /> </div>
                                    <div class="form-group"> <label class="control-label ">Email du directeur de votre
                                            structure d'accueil à l'étranger</label> <input type="email" name="emailE"
                                            class="form-control" /> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><br>
            </div> <br><button type="button" class="btn btn-outline-primary" onclick="prevPage()">Prev</button> <button
                type="button" class="btn btn-outline-primary" onclick="nextPage()">Next</button>
        </div>
        <div id="page-4" style="display: none;">
            <!-- form fields for page 4 go here -->
            <div class="container-fluid card"><br>
                <div class="container-fluid">
                    <h5>
                        <ul class="nav nav-tabs">
                            <li class="nav-item"> <a class="nav-link text-secondary opacity-50">Identite du candidat</a>
                            </li>
                            <li class="nav-item"> <a class="nav-link text-secondary opacity-50 ">Information sur la
                                    projet de recherche</a> </li>
                            <li class="nav-item"> <a class="nav-link text-secondary opacity-50 ">Information accueil</a>
                            </li>
                            <li class="nav-item"> <a class="nav-link text-info opacity-100 active"
                                    aria-current="page">Piece a joindre</a> </li>
                        </ul>
                    </h5>
                </div>
                <br>
                <div class="container spacer  col-xs-12 col-md-offset-3">
                    <div class="row">
                        <div class="col-md">
                            <div class="mb-3"> <label for="formFile" class="form-label" required>Photo</label> <input
                                    class="form-control" type="file" name="photo"> </div>
                            <div class="mb-3"> <label for="formFile" class="form-label">Copie de passport</label> <input
                                    class="form-control" type="file" name="passport"> </div>
                            <div class="mb-3"> <label for="formFile" class="form-label">Copie de votre diplôme de
                                    doctorat ou équivalent</label> <input class="form-control" type="file" id="formFile"
                                    name="dipDoctorat"> </div>
                        </div>
                        <div class="col-md">
                            <div class="mb-3"> <label for="formFile" class="form-label">CV avec mention de la liste de
                                    vos publications</label> <input class="form-control" type="file" name="cv"
                                    id="formFile">
                            </div>
                            <div class="mb-3"> <label for="formFile" class="form-label">Lettre d'invitation de la
                                    structure d'accueil avec mention des dates de séjour</label> <input
                                    class="form-control" type="file" id="formFile" name="lettreInv"> </div>

                            <div class="mb-3"> <label for="formFile" class="form-label">Autorisation d'absence de votre
                                    université ou de votre établissement</label> <input class="form-control" type="file"
                                    id="formFile" name="autorAbsence"> </div>
                        </div>
                    </div>
                </div>
                <p class="opacity-50">NB: les document doivent avoir le format pdf</p>
            </div> <br><button type="button" class="btn btn-outline-primary" onclick="prevPage()">Prev</button> <button
                type="submit" class="btn btn-outline-primary">Submit</button>

        </div>
    </form>
    <script>
        var currentPage = 1;

        function nextPage() {
            document.getElementById('page-' + currentPage).style.display = 'none';
            currentPage++;
            document.getElementById('page-' + currentPage).style.display = 'block';
        }

        function prevPage() {
            document.getElementById('page-' + currentPage).style.display = 'none';
            currentPage--;
            document.getElementById('page-' + currentPage).style.display = 'block';
        }
    </script>
</div>
<script>
    function isValidPDF(fileName) {
        var pattern = /[a-zA-Z0-9_-]+\.pdf$/;
        return pattern.test(fileName);
    }
    function validateForm() {
        var form = document.forms[0];
        for (var i = 0; i < form.elements.length; i++) {
            var element = form.elements[i];
            if (element.type === "file") {
                if (!isValidPDF(element.value)) {
                    alert("The selected file is not a valid PDF.");
                    return false;
                }
            }
        }
        return true;
    }
</script>