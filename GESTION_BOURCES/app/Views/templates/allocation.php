<style>
    .nav {
        display: flex;
        justify-content: space-between;
        align-items: center;
        justify-content: center;

    }

    .nav-link {
        flex: 0 0 auto;
    }

    .container {
        display: block;
        overflow: auto;
    }
</style>
<br>
<div class="container-fluid ">
    <h4>
        <ul class=" nav nav-tabs">
            <li class="nav-item"> <a href="/admin" class="nav-link text-info opacity-100 active"
                    aria-current="page">ALLOCATION DOCTORALE</a> </li>
            <li class="nav-item"> <a href="/jadmin" class="nav-link text-secondary opacity-50">SEJOUR
                    DE RECHERCHE JUNIOR</a> </li>
            <li class="nav-item"> <a href="#" class="nav-link text-secondary opacity-50 ">SEJOUR DE
                    RECHERCHE SENIOR
                </a> </li>
            <li class="nav-item"> <a href="#" class="nav-link text-secondary opacity-50">SEJOUR DOCTORAL
                </a> </li>
        </ul>
    </h4>
</div>
<br> <!-- Default box -->
<!-- form fields for page 1 go here -->
<div class="container-fluid me-1 ms-1">
    <table class="table table-bordered text-center">
        <thead>
            <div class="container-fluid">
                <ul class=" nav nav-tabs">
                    <li class="nav-item"> <a href="admin/" class="nav-link text-info opacity-100 active"
                            aria-current="page">infos</a> </li>
                    <li class="nav-item"> <a href="getfiles" class="nav-link text-secondary opacity-50">Voir les
                            fichiers</a> </li>
                    <li class="nav-item"> <a href="getencad" class="nav-link text-secondary opacity-50 ">info
                            encadrans
                        </a> </li>
                </ul>
            </div>
            <tr>
                <th>ID</th>
                <th>nom</th>
                <th>prenom</th>
                <th>dateNaissance</th>
                <th>email</th>
                <th>phone</th>
                <th>NNI</th>
                <th>genre</th>
                <th>adresse</th>
                <th>titre these</th>
                <th>resumer</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($allocation as $donnees => $row): ?>
                <tr class="col-3 outline-grid">
                    <td class="opacity-85 "><?= $row['id'] ?></td>
                    <td class="opacity-75">
                        <?= $row['nom'] ?>
                    </td>
                    <td class="opacity-75"><?= $row['prenom'] ?></td>
                    <td class="opacity-75">
                        <?= $row['dateNaissance'] ?>
                    </td>
                    <td class="opacity-75"><?= $row['email'] ?></td>
                    <td class="opacity-75">
                        <?= $row['telephone'] ?>
                    </td>
                    <td class="opacity-75"><?= $row['NNI'] ?></td>
                    <td class="opacity-75">
                        <?= $row['genre'] ?>
                    </td>
                    <td class="opacity-75"><?= $row['adresse'] ?></td>

                    <td class="opacity-75">
                        <?= $row['titrethese'] ?>
                    </td>
                    <td class="opacity-75">
                        <?= $row['resume'] ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div> <br> <!-- Default box --> <br>
</div>