<style>
    .nav {
        display: flex;
        justify-content: space-between;
        align-items: center;
        justify-content: center;

    }

    .nav-link {
        flex: 0 0 auto;
    }

    .container {
        display: block;
        overflow: auto;
    }
</style>
<br>
<div class="container-fluid ">
    <h4>
        <ul class=" nav nav-tabs">
            <li class="nav-item"> <a href="/admin" class="nav-link text-info opacity-100 active"
                    aria-current="page">ALLOCATION DOCTORALE</a> </li>
            <li class="nav-item"> <a href="/jadmin" class="nav-link text-secondary opacity-50">SEJOUR
                    DE RECHERCHE JUNIOR</a> </li>
            <li class="nav-item"> <a href="#" class="nav-link text-secondary opacity-50 ">SEJOUR DE
                    RECHERCHE SENIOR
                </a> </li>
            <li class="nav-item"> <a href="#" class="nav-link text-secondary opacity-50">SEJOUR DOCTORAL
                </a> </li>
        </ul>
    </h4>
</div>
<br> <!-- Default box -->
<!-- form fields for page 1 go here -->
<div class="container-fluid me-1 ms-1">
    <table class="table table-bordered text-center">
        <thead>
            <div class="container-fluid">
                <ul class=" nav nav-tabs">
                    <li class="nav-item"> <a href="/admin" class="nav-link text-secondary opacity-50"
                            aria-current="page">infos</a> </li>
                    <li class="nav-item"> <a href="getfiles" class="nav-link text-secondary opacity-50">Voir les
                            fichiers</a> </li>
                    <li class="nav-item"> <a href="getencad" class="nav-link text-info opacity-100 active ">info
                            encadrans
                        </a> </li>
                </ul>
            </div><thead>
                    <tr>
                        <th>ID</th>
                        <th>nom</th>
                        <th>prenom</th>
                        <th>Directeur M</th>
                        <th>grade M</th>
                        <th>email M</th>
                        <th>structure M</th>
                        <th>Directeur F</th>
                        <th>grade F</th>
                        <th>email F</th>
                        <th>structure F</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($allocation as $donnees => $row): ?>
                        <tr class="col-3 outline-grid">
                            <td class="opacity-85 "><?= $row['id'] ?></td>
                            <td class="opacity-75">
                                <?= $row['nom'] ?>
                            </td>
                            <td class="opacity-75"><?= $row['prenom'] ?></td>
                            <td class="opacity-75">
                                <?= $row['directeur'] ?>
                            </td>
                            <td class="opacity-75"><?= $row['grade'] ?></td>
                            <td class="opacity-75">
                                <?= $row['emailD'] ?>
                            </td>
                            <td class="opacity-75"><?= $row['affilation'] ?></td>
                            <td class="opacity-75">
                                <?= $row['directeurF'] ?>
                            </td>
                            <td class="opacity-75"><?= $row['gradeF'] ?></td>

                            <td class="opacity-75">
                                <?= $row['emailF'] ?>
                            </td>
                            <td class="opacity-75">
                                <?= $row['structure'] ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div> <br> <!-- Default box --> <br>
</div>