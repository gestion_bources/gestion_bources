<style>
    .nav {
        display: flex;
        justify-content: space-between;
        align-items: center;
        justify-content: center;

    }

    .nav-link {
        flex: 0 0 auto;
    }

    .container {
        display: block;
        overflow: auto;
    }
</style>
<br>
<div class="container-fluid ">
    <h4>
        <ul class=" nav nav-tabs">
            <li class="nav-item"> <a href="/admin" class="nav-link text-secondary opacity-50"
                    aria-current="page">ALLOCATION DOCTORALE</a> </li>
            <li class="nav-item"> <a href="/jadmin" class="nav-link text-info opacity-100 active">SEJOUR
                    DE RECHERCHE JUNIOR</a> </li>
            <li class="nav-item"> <a href="#" class="nav-link text-secondary opacity-50 ">SEJOUR DE
                    RECHERCHE SENIOR
                </a> </li>
            <li class="nav-item"> <a href="#" class="nav-link text-secondary opacity-50">SEJOUR DOCTORAL
                </a> </li>
        </ul>
    </h4>
</div>
<br> <!-- Default box -->
<!-- form fields for page 1 go here -->
<div class="container-fluid me-1 ms-1">
    <table class="table table-bordered text-center">
        <thead>
            <div class="container-fluid">
                <ul class=" nav nav-tabs">
                    <li class="nav-item"> <a href="jadmin" class="nav-link text-secondary opacity-50"
                            aria-current="page">infos</a> </li>
                    <li class="nav-item"> <a href="jgetfiles" class="nav-link text-secondary opacity-50">Voir les
                            fichiers</a> </li>
                    <li class="nav-item"> <a href="jgetencad" class="nav-link text-info opacity-100 active ">info
                            encadrans
                        </a> </li>
                </ul>
            </div>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>nom</th>
                    <th>prenom</th>
                    <th>projet recherche</th>
                    <th>resumer recherche</th>
                    <th>motif</th>
                    <th>nom srm</th>
                    <th>nom sam</th>
                    <th>directeur </th>
                    <th>emailD </th>
                </tr>
            </thead>
        <tbody>
            <?php foreach ($junior as $donnees => $row): ?>
                <tr class="col-3 outline-grid">
                    <td class="opacity-85 "><?= $row['id'] ?></td>
                    <td class="opacity-75">
                        <?= $row['nom'] ?>
                    </td>
                    <td class="opacity-75"><?= $row['prenom'] ?></td>
                    <td class="opacity-75">
                        <?= $row['projetR'] ?>
                    </td>
                    <td class="opacity-75"><?= $row['resumePR'] ?></td>
                    <td class="opacity-75">
                        <?= $row['motif'] ?>
                    </td>
                    <td class="opacity-75"><?= $row['nomSRM'] ?></td>
                    <td class="opacity-75">
                        <?= $row['nomSAE'] ?>
                    </td>
                    <td class="opacity-75"><?= $row['directeurE'] ?></td></td>
                    <td class="opacity-75"><?= $row['emailE'] ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div> <br> <!-- Default box --> <br>
</div>