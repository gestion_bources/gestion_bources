<style>
    .nav {
        display: flex;
        justify-content: space-between;
        align-items: center;
        justify-content: center;

    }

    .nav-link {
        flex: 0 0 auto;
    }

    .container {
        display: block;
        overflow: auto;
    }
</style>
<br>
<div class="container-fluid ">
    <h4>
        <ul class=" nav nav-tabs">
            <li class="nav-item"> <a href="/admin" class="nav-link text-secondary opacity-50"
                    aria-current="page">ALLOCATION DOCTORALE</a> </li>
            <li class="nav-item"> <a href="/jadmin" class="nav-link text-info opacity-100 active">SEJOUR
                    DE RECHERCHE JUNIOR</a> </li>
            <li class="nav-item"> <a href="#" class="nav-link text-secondary opacity-50 ">SEJOUR DE
                    RECHERCHE SENIOR
                </a> </li>
            <li class="nav-item"> <a href="#" class="nav-link text-secondary opacity-50">SEJOUR DOCTORAL
                </a> </li>
        </ul>
    </h4>
</div>
<br> <!-- Default box -->
<!-- form fields for page 1 go here -->
<div class="container-fluid me-1 ms-1">
    <table class="table table-bordered text-center">
        <thead>
            <div class="container-fluid">
                <ul class=" nav nav-tabs">
                    <li class="nav-item"> <a href="/jadmin" class="nav-link text-secondary opacity-50"
                            aria-current="page">infos</a> </li>
                    <li class="nav-item"> <a href="jgetfiles" class="nav-link text-info opacity-100 active">Voir les
                            fichiers</a> </li>
                    <li class="nav-item"> <a href="jgetencad" class="nav-link text-secondary opacity-50 ">info
                            encadrans
                        </a> </li>
                </ul>
            </div>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>nom</th>
                    <th>prenom</th>
                    <th>photo</th>
                    <th>passport</th>
                    <th>diplome doctoral</th>
                    <th>CV</th>
                    <th>lettre invitation</th>
                    <th>autorisation d'absence</th>
                    <th>AllFiles(zip)</th>
                </tr>
            </thead>
        <tbody>
            <?php foreach ($junior as $donnees => $row): ?>
                <tr class="col-3 outline-grid">
                    <td class="opacity-85"><?= $row['id'] ?></td>
                    <td class="opacity-75">
                        <?= $row['nom'] ?>
                    </td>
                    <td class="opacity-75"><?= $row['prenom'] ?></td>
                    <td><a href="jdownload/photo/<?= $row['id'] ?>"><i class="bi bi-file-earmark-arrow-down"></i></a>
                    </td>
                    <td><a href="jdownload/passport/<?= $row['id'] ?>"><i class="bi bi-file-earmark-arrow-down"></i></a>
                    </td>
                    <td><a href="jdownload/dipDoctorat/<?= $row['id'] ?>"><i class="bi bi-file-earmark-arrow-down"></i></a>
                    </td>
                    <td><a href="jdownload/cv/<?= $row['id'] ?>"><i class="bi bi-file-earmark-arrow-down"></i></a></td>
                    <td><a href="jdownload/lettreInv/<?= $row['id'] ?>"><i class="bi bi-file-earmark-arrow-down"></i></a>
                    </td>
                    <td><a href="jdownload/autorAbsence/<?= $row['id'] ?>"><i class="bi bi-file-earmark-arrow-down"></i></a>
                    </td>
                    <td><a href="jdownloadZip/<?= $row['id'] ?>"><i class="bi bi-file-earmark-arrow-down"></i></a>
                    </td>

                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div> <br> <!-- Default box --> <br>
</div>