<?php

namespace App\Models;

use CodeIgniter\Model;

class Allocation_Model extends Model
{


    protected $table = 'User';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'nom_complet',
        'pass',
        'email',
        'username'
    ];

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;


    /**
     * @return mixed
     */
    public function getAllowedFields()
    {
        return $this->allowedFields;
    }

    /**
     * @param mixed $allowedFields 
     * @return self
     */
    public function setAllowedFields($allowedFields): self
    {
        $this->allowedFields = $allowedFields;
        return $this;
    }
    public function getId()
    {
        return $this->primaryKey;
    }
}