<?php

namespace App\Models;

use CodeIgniter\Model;

class Junior_Model extends Model
{


	protected $table = 'Junior';
	protected $primaryKey = 'id';

	protected $useAutoIncrement = true;

	protected $returnType = 'array';
	protected $useSoftDeletes = true;

	protected $allowedFields = [
		'nom',
		'prenom',
		'NNI',
		'genre',
		'dateNaissance',
		'email',
		'adresse',
		'telephone',
		'projetR',
		'resumePR',
		'motif',
		'nomSRM',
		'nomSAE',
		'directeurE',
		'emailE',
		'photo',
		'passport',
		'dipDoctorat',
		'cv',
		'lettreInv',
		'autorAbsence'
	];

	protected $useTimestamps = true;
	protected $createdField = 'date_creation';
	protected $updatedField = 'date_modification';
	protected $deletedField = 'date_suppression';

	protected $validationRules = [];
	protected $validationMessages = [];
	protected $skipValidation = false;


	/**
	 * @return mixed
	 */
	public function getAllowedFields()
	{
		return $this->allowedFields;
	}

	/**
	 * @param mixed $allowedFields 
	 * @return self
	 */
	public function setAllowedFields($allowedFields): self
	{
		$this->allowedFields = $allowedFields;
		return $this;
	}
	public function getId()
	{
		return $this->primaryKey;
	}
}