<?php

namespace Config;

use App\Controllers\Inscrire;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (is_file(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

$routes->get('/', 'Home::index');
$routes->get('/home', 'Home::index');
$routes->get('/message', 'Message::index');

$routes->get('/allocation', 'AllocationDoctorat::index');
 $routes->get('/junior', 'Junior::index');
//$routes->get('/login', 'Login::index');
 $routes->get('/junior', 'SejourJunior::index');
$routes->get('/login', 'Login::index');
 $routes->get('/signup', 'SignUp::index');


$routes->post('ajout', 'AllocationDoctorat::ajout');

$routes->get('admin', 'AllocationDoctorat::voir');
$routes->get('getencad', 'AllocationDoctorat::encad');
$routes->get('getfiles', 'AllocationDoctorat::voirfiles');

$routes->get('download/(:any)', 'AllocationDoctorat::downloadFile/$1/$2');
$routes->get('downloadZip/(:any)', 'AllocationDoctorat::downloadZipFile/$1');

$routes->get('product/(:num)', 'Catalog::productLookupByID/$1');
$routes->match(['get','post'],'login', 'Login::index');
$routes->get('dashboard', 'Dashboard::index');
$routes->get('logout', 'Login::logout');
$routes->post('/jajout', 'SejourJunior::ajout');
$routes->get('/jadmin', 'SejourJunior::voir');
$routes->get('/jgetfiles', 'SejourJunior::voirfiles');
$routes->get('jgetencad', 'SejourJunior::encad');

$routes->get('/jdownload/(:any)', 'SejourJunior::downloadFile/$1/$2');
$routes->get('/jdownloadZip/(:any)', 'SejourJunior::downloadZipFile/$1');

$routes->match(['get','post'],'register', 'Login::register');



/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
