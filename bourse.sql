-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 05, 2023 at 06:35 PM
-- Server version: 10.4.25-MariaDB
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bourse`
--

-- --------------------------------------------------------

--
-- Table structure for table `Candidat`
--

CREATE TABLE `Candidat` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `NNI` varchar(20) DEFAULT NULL,
  `genre` varchar(10) DEFAULT NULL,
  `dateNaissance` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `titrethese` varchar(255) DEFAULT NULL,
  `resume` varchar(255) DEFAULT NULL,
  `directeur` varchar(255) DEFAULT NULL,
  `grade` varchar(255) DEFAULT NULL,
  `emailD` varchar(255) DEFAULT NULL,
  `directeurF` varchar(255) DEFAULT NULL,
  `gradeF` varchar(255) DEFAULT NULL,
  `emailF` varchar(255) DEFAULT NULL,
  `affilation` varchar(255) DEFAULT NULL,
  `structure` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `passport` varchar(255) DEFAULT NULL,
  `attestation` varchar(255) DEFAULT NULL,
  `masterDip` varchar(255) DEFAULT NULL,
  `noteLM` varchar(255) DEFAULT NULL,
  `attestB2` varchar(255) DEFAULT NULL,
  `lettreEng` varchar(255) DEFAULT NULL,
  `cvDoctorat` varchar(255) DEFAULT NULL,
  `resumer` varchar(255) DEFAULT NULL,
  `infoThese` varchar(255) DEFAULT NULL,
  `date_creation` timestamp NULL DEFAULT current_timestamp(),
  `date_modification` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_suppression` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Candidat`
--

INSERT INTO `Candidat` (`id`, `nom`, `prenom`, `NNI`, `genre`, `dateNaissance`, `email`, `adresse`, `telephone`, `titrethese`, `resume`, `directeur`, `grade`, `emailD`, `directeurF`, `gradeF`, `emailF`, `affilation`, `structure`, `photo`, `passport`, `attestation`, `masterDip`, `noteLM`, `attestB2`, `lettreEng`, `cvDoctorat`, `resumer`, `infoThese`, `date_creation`, `date_modification`, `date_suppression`) VALUES
(31, 'M\'haimid', 'Abdallahi Salem', '5555', 'homme', '2023-01-27', 'abscheik@gmail.com', 'Aravat, Nouakchott, Mauritania', '+22234837080', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'text/x-java', 'application/vnd.rar', 'image/jpeg', 'text/html', 'application/pdf', 'text/html', 'application/x-php', NULL, NULL, '2023-01-05 07:23:23', '2023-01-05 07:23:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Files`
--

CREATE TABLE `Files` (
  `id` int(11) NOT NULL,
  `photo` blob NOT NULL,
  `passport` blob NOT NULL,
  `attestation` blob NOT NULL,
  `masterDip` blob NOT NULL,
  `noteLM` blob NOT NULL,
  `attestB2` blob NOT NULL,
  `lettreEng` blob NOT NULL,
  `cvDoctorat` blob NOT NULL,
  `resumer` blob NOT NULL,
  `infoThese` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Candidat`
--
ALTER TABLE `Candidat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Files`
--
ALTER TABLE `Files`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Candidat`
--
ALTER TABLE `Candidat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `Files`
--
ALTER TABLE `Files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Files`
--
ALTER TABLE `Files`
  ADD CONSTRAINT `Files_ibfk_1` FOREIGN KEY (`id`) REFERENCES `Candidat` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
